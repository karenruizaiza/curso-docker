import {Component, OnInit, ViewChild} from '@angular/core';
import {DepartmentService} from '../../services/department.service';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {EmployeeService} from '../../services/employee.service';
import {Employee} from '../../models/employee';
import {concatMap, exhaustMap, first, flatMap, map, mergeAll, switchMap} from 'rxjs/operators';
import {pipe, zip} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ConfirmDialogModel, DialogComponent} from '../dialog/dialog.component';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  employees: Employee[];
  currentDepartment = null;
  currentIndex = -1;
  name = '';

  displayedColumns: string[] = ['deparmentName', 'name', 'age', 'position', 'options'];
  dataSource = new MatTableDataSource<Employee>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: DepartmentService,
              private employeeService: EmployeeService,
              public dialog: MatDialog) {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.retrieve();
  }

  delete(id: string) {
    const message = `Esta seguro que desea eliminar este elemento?`;

    const dialogData = new ConfirmDialogModel('Eliminar', message);

    const dialogRef = this.dialog.open(DialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().pipe(
      switchMap((res) => {
        if (res) {
          return this.employeeService.delete(id);
        }
      })
    ).subscribe(() => this.refreshList());
  }

  retrieve() {
    this.employeeService.getAll().pipe(
      exhaustMap((employees) => {
        return zip(...employees.map((employee) => {
          return this.service.get(employee.department ? employee.department.id : null).pipe(
            map((dept) => {
              employee.department = dept;
              return employee;
              }),
            );
          })
        );
      }))
      .subscribe(
        data => {
          this.dataSource.data = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList() {
    this.retrieve();
  }

  searchName(name: string) {
    this.dataSource.filter = name.trim().toLowerCase();
  }

}
